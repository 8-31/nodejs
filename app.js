import http from 'http';
import path from 'path'
import express from 'express';
import bodyParser from 'body-parser';
import { fileURLToPath } from 'url';
import misrutas from './router/index.js';
 // Import the router correctly

const app = express();
const port = 80;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Configurar el motor de vistas EJS
app.set('view engine', 'ejs');

// Middleware para manejar datos URL-encoded
app.use(bodyParser.urlencoded({ extended: true }));

// Servir archivos estáticos desde el directorio 'public'
app.use(express.static(__dirname + '/public'));

// Use the router imported from './router/index.js'
app.use(misrutas); // Assuming misrutas is an object with a router property

// Iniciar el servidor
app.listen(port, () => {
    console.log("Iniciando el servidor en el puerto " + port);
});
