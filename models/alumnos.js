import { resolve } from "path";
import conexion from "./conexion.js";

const AlumnosDb = {}; // Renombrado a AlumnosDb y cambiado a const

AlumnosDb.insertar = function insertar(alumno) {
    return new Promise((resolve, reject) => { // Cambiado rejects a reject
        let sqlConsulta = "Insert into alumnos set ?";
        conexion.query(sqlConsulta, alumno, function (err, res) {
            if (err) {
                console.log("Surgio un error ", err.message);
                reject(err); // Cambiado rejects a reject
            } else {
                const alumno = {
                    id: res.insertId,
                };
                resolve(alumno);
            }
        });
    });
};

AlumnosDb.mostrarTodos = function mostrarTodos() {
    return new Promise((resolve, reject) => {
        let sqlConsulta = "select * from alumnos";
        conexion.query(sqlConsulta, null, function (err, res) {
            if (err) {
                console.log("Surgio un error", err.message); // Corregido mensaje de error
                reject(err);
            } else {
                resolve(res);
            }
        });
    });
};

AlumnosDb.consultarMatricula = function consultarMatricula(matricula){
    return new promiseImpl((resolve,reject) =>{
        var sqlConsulta= "select * from alumnos where matricula = ?";
         conexion.query(sqlConsulta,[matricula], function(err, res){
            if (err) {
                console.log("Surgio un error", err.message); 
                reject(err);
            } else {
                resolve(res);
            }
         });
    });
}

AlumnosDb.borrarPorMatricula = function borrarPorMatricula(matricula){
    return new promiseImpl((resolve,reject) =>{
        var sqlConsulta= "delete from alumnos where matricula = ?";
         conexion.query(sqlConsulta,[matricula], function(err, res){
            if (err) {
                console.log("Surgio un error", err.message); 
                reject(err);
            } else {
                resolve(res);
            }
         });
    });
}

AlumnosDb.actualizarPorMatricula = function actualizarPorMatricula(matricula, alumno) {
    return new Promise((resolve, reject) => {
        let sqlConsulta = "UPDATE alumnos SET ? WHERE matricula = ?";
        conexion.query(sqlConsulta, [alumno, matricula], function (err, res) {
            if (err) {
                console.log("Surgió un error: ", err.message);
                reject(err);
            } else {
                console.log("Alumno actualizado correctamente.");
                resolve(res);
            }
        });
    });
};

export default AlumnosDb;
