import express from "express";
import bodyParser from "body-parser";
import conexion from "../models/conexion.js";
import AlumnosDb from "../models/alumnos.js";

const router = express.Router();


router.use(bodyParser.urlencoded({ extended: true }));

// Ruta principal
router.get('/', (req, res) => {
    res.render('index', { titulo: "Mi Primer Pagina en ejs", nombre: "Axel Jovani Ruiz Guerrero" });
});

// Ruta para la tabla (GET y POST)
router.get('/tabla', (req, res) => {
    const numero = req.query.numero;
    res.render('tabla', { numero: numero });
});

router.post('/tabla', (req, res) => {
    const numero = req.body.numero;
    res.render('tabla', { numero: numero });
});

router.post('/cotizacion', (req, res) => {
    const { descripcion, precio, porcentajeInicial, plazo } = req.body;


    const precioAutomovil = parseFloat(precio);
    const porcentaje = parseFloat(porcentajeInicial) / 100;
    const meses = parseInt(plazo);

    const pagoInicial = precioAutomovil * porcentaje;
    const totalFinanciar = precioAutomovil - pagoInicial;
    const pagoMensual = totalFinanciar / meses;

    res.render('cotizacion', {
        descripcion,
        precio: precioAutomovil,
        porcentajeInicial: porcentaje,
        plazo: meses,
        pagoInicial,
        totalFinanciar,
        pagoMensual
    });
});

router.get('/cotizacion', (req, res) => {
    res.render('cotizacion', {
        descripcion: "",
        precio: 0,
        porcentajeInicial: 0,
        plazo: 0,
        pagoInicial: 0,
        totalFinanciar: 0,
        pagoMensual: 0
    });
});

router.get('/alumnos', async (req, res) => {
    try {
        const rows = await AlumnosDb.mostrarTodos();
        res.render('alumnos', { reg: rows });
    } catch (error) {
        console.error(error);
        res.status(500).send("Ocurrió un error al obtener los alumnos.");
    }
});

router.post('/alumnos', async (req, res) => {
    try {
        const matricula = req.body.matricula;
        const nombre = req.body.nombre;
        const domicilio = req.body.domicilio;
        const sexo = req.body.sexo.toUpperCase();
        const especialidad = req.body.especialidad;

        const rows = await AlumnosDb.mostrarTodos();
        const matriculas = rows.map(row => row.matricula);
        if (matriculas.includes(matricula)) {
            alert('La matrícula ingresada ya existe');
            return;
        }

        // Validar sexo
        if (sexo !== 'M' && sexo !== 'F') {
            alert('El sexo debe ser M o F');
            return;
        }

        const params = { matricula, nombre, domicilio, sexo, especialidad };
        await AlumnosDb.insertar(params);
        alert('Alumno insertado correctamente');
    } catch (error) {
        console.error(error);
        alert('Ocurrió un error al insertar el alumno.');
    }
});

router.post("/buscar", async (req, res) => {
    matricula = req.body.matricula;
    row = await AlumnosDb.consultarMatricula(matricula);
    res.render("alumnos", { alu: row });

})

// Ruta para mostrar la página de prácticas (GET)
router.get('/Practicas', (req, res) => {
    // Renderizar la vista de prácticas
    res.render('Practicas', { titulo: "Prácticas para el examen" });
});

// Ruta para mostrar la página de preparación para el examen (GET)
router.get('/preExamen', (req, res) => {
    // Renderizar la vista de preparación para el examen
    res.render('preExamen', { titulo: "Preparación para el examen" });
});

// Ruta para manejar el formulario y mostrar la página de pago de servicio
router.post('/preExamen', (req, res) => {
    try {
        // Recibir los datos del formulario
        const numeroRecibo = req.body.numeroRecibo;
        const nombre = req.body.nombre;
        const domicilio = req.body.domicilio;
        const tipoServicio = req.body.tipoServicio;
        const kilowatts = parseInt(req.body.kilowatts);

        // Validar datos (opcional)
        if (!numeroRecibo || !nombre || !domicilio || !tipoServicio || isNaN(kilowatts)) {
            throw new Error("Por favor, complete todos los campos correctamente.");
        }

        // Calcular el costo por kilowatts según el tipo de servicio
        let costoPorKilowatt;
        switch (tipoServicio) {
            case '1':
                costoPorKilowatt = 1.08;
                break;
            case '2':
                costoPorKilowatt = 2.5;
                break;
            case '3':
                costoPorKilowatt = 3.0;
                break;
            default:
                throw new Error("Tipo de servicio inválido.");
        }

        // Calcular subtotal
        const subtotal = costoPorKilowatt * kilowatts;

        // Calcular impuesto (16% del subtotal)
        const impuesto = subtotal * 0.16;

        // Calcular descuento
        let descuento;
        if (kilowatts <= 1000) {
            descuento = subtotal * 0.1;
        } else if (kilowatts > 1000 && kilowatts <= 10000) {
            descuento = subtotal * 0.2;
        } else {
            descuento = subtotal * 0.5;
        }

        // Calcular total a pagar
        const total = subtotal + impuesto - descuento;

        // Renderizar la página de pago de servicio con los datos calculados
        res.render('Pago', {
            numeroRecibo: numeroRecibo,
            nombre: nombre,
            domicilio: domicilio,
            tipoServicio: tipoServicio,
            costoPorKilowatt: costoPorKilowatt,
            subtotal: subtotal,
            impuesto: impuesto,
            descuento: descuento,
            total: total,
            mensaje: "¡Datos calculados con éxito!"
        });
    } catch (error) {
        // Si hay un error, mostrar mensaje de error en la vista
        res.render('error', { mensaje: error.message });
    }
});

router.post('/examen', (req, res) => {
    const { numDocente, nombre, domicilio, nivel, pagoBase, horasImpartidas, numHijos } = req.body;

    const esnumDocentePositivo = (numDocente) => {
        return !isNaN(parseFloat(numDocente)) && isFinite(numDocente) && parseInt(numDocente) >= 0;
    };
    
    const esNumeroPositivo = (valor) => {
        return !isNaN(parseFloat(valor)) && isFinite(valor) && parseFloat(valor) >= 0;
    };

    if(!esnumDocentePositivo(numDocente)){
        return res.status(400).send('el numero del decente debe ser positivo');
    }

    if (!esNumeroPositivo(pagoBase) || !esNumeroPositivo(horasImpartidas)) {
        return res.status(400).send('El pago base y las horas impartidas deben ser números positivos.');
    }
    
    if (!isNaN(nombre)) {
        return res.status(400).send('El nombre no puede contener solo números.');
    }

    const calcularPagoBase = (nivel, pagoBase) => {
        let incremento;
        switch (nivel) {
            case '1':
                incremento = 0.3;
                break;
            case '2':
                incremento = 0.5;
                break;
            case '3':
                incremento = 1.0;
                break;
            default:
                incremento = 0;
        }
        return parseFloat(pagoBase) * (1 + incremento);
    };

    const calcularImpuesto = (pagoTotal) => {
        return pagoTotal * 0.16;
    };

    const calcularBono = (numHijos) => {
        switch (numHijos) {
            case '0':
                return 0;
            case '1-2':
                return 0.05;
            case '3-5':
                return 0.1;
            case 'más de 5':
                return 0.2;
            default:
                return 0;
        }
    };

    const pagoBaseIncrementado = calcularPagoBase(nivel, pagoBase);
    const pagoTotal = pagoBaseIncrementado * parseFloat(horasImpartidas);
    const impuesto = calcularImpuesto(pagoTotal);
    const bono = calcularBono(numHijos);
    const bonoTotal = pagoTotal * bono;
    const pagoTotalConBono = pagoTotal + bonoTotal - impuesto;

    res.render('recibo', {
        pagoTotal,
        impuesto,
        bonoTotal,
        pagoTotalConBono
    });
});

router.get('/examen', (req, res) => {
    res.render('examen', { titulo: "examen" });
});

router.get('/recibo', (req, res) => {
    // Aquí puedes definir o recuperar las variables que deseas pasar a la plantilla recibo.ejs
    const pagoTotal = 0 ;
    const impuesto = 0;
    const bonoTotal = 0;
    const pagoTotalConBono = 0 ;

    res.render('recibo', {
        titulo: "Recibo de Examen",
        pagoTotal,
        impuesto,
        bonoTotal,
        pagoTotalConBono
    });
});


const tiposServicio = {
    '1': 'Domicilio',
    '2': 'Comercial',
    '3': 'Industrial'
};

// Ruta para mostrar la página de pago de servicio (GET)
router.get('/Pago', (req, res) => {
    try {
        // Obtener el tipo de servicio del query string
        const tipoServicio = req.query.tipoServicio || "";

        // Obtener el nombre del tipo de servicio del objeto tiposServicio
        const nombreTipoServicio = tiposServicio[tipoServicio] || "";

        // Renderizar la página de pago de servicio con los datos calculados y el nombre del tipo de servicio
        res.render('Pago', {
            nombre: "",
            tipoServicio: tipoServicio,
            kilowatts: 0,
            costoPorKilowatt: 0,
            numeroRecibo: "",
            domicilio: "",
            subtotal: 0,
            impuesto: 0,
            descuento: 0,
            total: 0,
            mensaje: "",
            nombreTipoServicio: nombreTipoServicio // Pasar el nombre del tipo de servicio
        });
    } catch (error) {
        // Si hay un error, mostrar mensaje de error en la vista
        res.render('error', { mensaje: error.message });
    }
});

// Ruta para manejar el formulario de pago y mostrar la página de confirmación (POST)
router.post('/Pago', (req, res) => {
    try {
        // Recibir los datos del formulario de pago
        const numeroRecibo = req.body.numeroRecibo;
        const nombre = req.body.nombre;
        const domicilio = req.body.domicilio;
        const tipoServicio = req.body.tipoServicio;
        const kilowatts = parseInt(req.body.kilowatts);

        // Validar los datos recibidos (opcional)
        if (!numeroRecibo || !nombre || !domicilio || !tipoServicio || isNaN(kilowatts)) {
            throw new Error("Por favor, complete todos los campos correctamente.");
        }

        // Calcular el costo por kilowatts según el tipo de servicio
        let costoPorKilowatt;
        switch (tipoServicio) {
            case '1':
                costoPorKilowatt = 1.08;
                break;
            case '2':
                costoPorKilowatt = 2.5;
                break;
            case '3':
                costoPorKilowatt = 3.0;
                break;
            default:
                throw new Error("Tipo de servicio inválido.");
        }

        // Calcular subtotal
        const subtotal = costoPorKilowatt * kilowatts;

        // Calcular impuesto (16% del subtotal)
        const impuesto = subtotal * 0.16;

        // Calcular descuento
        let descuento;
        if (kilowatts <= 1000) {
            descuento = subtotal * 0.1;
        } else if (kilowatts > 1000 && kilowatts <= 10000) {
            descuento = subtotal * 0.2;
        } else {
            descuento = subtotal * 0.5;
        }

        // Calcular total a pagar
        const total = subtotal + impuesto - descuento;

        // Renderizar la página de confirmación de pago con los datos calculados
        res.render('Pago', {
            nombre: nombre,
            tipoServicio: tipoServicio,
            kilowatts: kilowatts,
            costoPorKilowatt: costoPorKilowatt,
            numeroRecibo: numeroRecibo,
            domicilio: domicilio,
            subtotal: subtotal,
            impuesto: impuesto,
            descuento: descuento,
            total: total,
            mensaje: "¡Pago realizado con éxito!"
        });
    } catch (error) {
        // Si hay un error, mostrar mensaje de error en la vista
        res.render('error', { mensaje: error.message });
    }
});


export default router;